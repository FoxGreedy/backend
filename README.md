# Teste de desenvolvimento Back End Node.js

[![N|Solid](https://pixter.com.br/wp-content/uploads/2018/06/logo-black.png)](http://pixter.com.br)

## How To Do

### Install Dependencies

```
  npm install
```

### Start

```
  npm start
```

### Routes

```
  POST http://localhost:3333/gas_station
```

#### Request Body

```
  {
        "lat": "-23.5655625",
        "lng": "-46.6472389"
  }
```
