const axios = require("axios");

const { keyMaps, baseURLMapsPlace: placeURL } = require("../utils/constants");

const gasStations = (lat, lng) =>
  new Promise((resolve, reject) => {
    axios
      .get(
        `${placeURL}${lat},${lng}&radius=10000&type=gas_station&key=${keyMaps}`
      )
      .then(function (response) {
        const { results } = response.data;

        const gas_stations = results.map(
          (result) =>
            new Object({
              ...result.geometry.location,
              nome: result.name,
              endereco: result.vicinity,
            })
        );

        resolve(gas_stations);
      })
      .catch((error) => {
        reject(error);
      });
  });

module.exports = gasStations;
