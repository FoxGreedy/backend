const axios = require("axios");

const {
  keyMaps,
  addressMapping,
  baseURLMapsGeocode: geocodeURL,
} = require("../utils/constants");

const address = (lat, lng) =>
  new Promise((resolve, reject) => {
    axios
      .get(`${geocodeURL}${lat},${lng}&sensor=false&key=${keyMaps}`)
      .then(function (response) {
        const { results } = response.data;

        const [{ address_components } = data] = results.filter(
          ({ geometry: { location = undefined } }) =>
            location && location.lat == lat && location.lng == lng
        );

        const infosAddress = address_components.map((element) => {
          const [key = undefined] = element.types.filter((type) =>
            Object.keys(addressMapping).includes(type)
          );

          return key && new Object({ [addressMapping[key]]: element.short_name });
        });

        resolve(Object.assign(...infosAddress));
      })
      .catch((error) => {
        reject(error);
      });
  });

module.exports = address;
