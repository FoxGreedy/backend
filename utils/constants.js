module.exports = {
  baseURLMapsGeocode:
    "https://maps.googleapis.com/maps/api/geocode/json?latlng=",
  baseURLMapsPlace:
    "https://maps.googleapis.com/maps/api/place/search/json?location=",

  keyMaps: "AIzaSyA6TfU84r6wT2gu1NYAOCN7JkO342K21So",

  addressMapping: {
    street_number: "numero",
    route: "logradouro",
    sublocality_level_1: "bairro",
    administrative_area_level_2: "cidade",
    administrative_area_level_1: "estado",
    country: "pais",
    postal_code: "CEP",
  },
};
