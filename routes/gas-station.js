const router = require("express").Router();
const axios = require("axios");

router.post("/", (req, res) => {
  const { lat, lng } = req.body;

  const address = require("../controllers/addressController")(lat, lng);
  const gasStations = require("../controllers/gasStationsController")(lat, lng);

  Promise.all([address, gasStations])
    .then((response) => {
      const [add, gas] = response;

      res.json(
        new Object({
          lat,
          lng,
          ...add,
          postos: gas,
        })
      );
    })
    .catch((response) => {
      res.json(response);
    });
});

module.exports = router;
