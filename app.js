const createError = require('http-errors')
const express = require('express')
const path = require('path')
const logger = require('morgan')

//Configurando a estrutura da aplicação
var app = express()
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, 'public')))

app.use((req, res, next) => {
    next()
})

const routerGasStation = require('./routes/gas-station')

app.use('/gas_station', routerGasStation)

app.use(function (req, res, next) {
    next(createError(404))
})

app.use((err, req, res, next) => {
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    res.status(err.status || 500)
    res.render('error', {
        error: err.message
    })
})

module.exports = app